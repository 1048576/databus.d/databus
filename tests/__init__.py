import glob
import os.path
import sys

root_dir: str = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))

for pkg_path in glob.glob("{}/pkgs/*/".format(root_dir)):
    sys.path.insert(0, pkg_path)
