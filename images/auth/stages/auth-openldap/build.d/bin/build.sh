#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

_sh "rm -rf /opt/bitnami/openldap/var/run/"
_sh "ln -sf /mnt/run /opt/bitnami/openldap/var/run"

# Post install
_sh "rm $0"
