#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

while ! test -S /var/run/ldapi; do
    echo "Waiting for openldap server to start: $(date)"
    sleep 2
done

echo "Openldap server started"

while ! test -e /etc/krb5kdc/service.keyfile; do
    echo "Waiting for initialization to complete: $(date)"
    sleep 2
done

echo "Initialization completed"

exec "$@"
