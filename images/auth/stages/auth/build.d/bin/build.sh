#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

_sh "mkdir -p /mnt/run/"
_sh "mkdir -p /mnt/krb5kdc/"

_sh "chown -R ${BUILD_ARG_APP_USER_ID} /mnt/*"

# Post install
_sh "rm $0"
