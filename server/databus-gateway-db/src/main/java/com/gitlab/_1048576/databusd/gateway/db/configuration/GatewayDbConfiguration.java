package com.gitlab._1048576.databusd.gateway.db.configuration;

import com.gitlab._1048576.databusd.gateway.db.query.GatewayDbQuery;

public interface GatewayDbConfiguration {
    public GatewayDbQuery query();
}
