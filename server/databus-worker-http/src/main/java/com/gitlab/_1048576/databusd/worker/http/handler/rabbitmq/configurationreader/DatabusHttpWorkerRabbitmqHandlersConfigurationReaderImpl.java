package com.gitlab._1048576.databusd.worker.http.handler.rabbitmq.configurationreader;

import com.gitlab._1048576.databusd.worker.handler.rabbitmq.DatabusWorkerRabbitmqQueueHandlerConfiguration;
import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerEndpointConfiguration;
import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerKeytabConfiguration;
import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerRabbitmqHandlerConfiguration;
import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerRabbitmqHandlersConfiguration;
import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerRabbitmqHandlersConfigurationReader;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class DatabusHttpWorkerRabbitmqHandlersConfigurationReaderImpl implements DatabusHttpWorkerRabbitmqHandlersConfigurationReader {
    @Override
    public DatabusHttpWorkerRabbitmqHandlersConfiguration read(
        final Iterable<DatabusWorkerRabbitmqQueueHandlerConfiguration> queues,
        final Map<String, DatabusHttpWorkerEndpointConfiguration> endpoints,
        final Map<String, DatabusHttpWorkerKeytabConfiguration> keytabs
    ) throws Exception {
        final var handlers = this.createHandlers(
            queues,
            endpoints,
            keytabs
        );

        return new DatabusHttpWorkerRabbitmqHandlersConfiguration() {
            @Override
            public Iterable<DatabusHttpWorkerRabbitmqHandlerConfiguration> handlers() {
                return Collections.unmodifiableList(handlers);
            }
        };
    }

    private List<DatabusHttpWorkerRabbitmqHandlerConfiguration> createHandlers(
        final Iterable<DatabusWorkerRabbitmqQueueHandlerConfiguration> queues,
        final Map<String, DatabusHttpWorkerEndpointConfiguration> endpoints,
        final Map<String, DatabusHttpWorkerKeytabConfiguration> keytabs
    ) throws Exception {
        final var handlers = new ArrayList<DatabusHttpWorkerRabbitmqHandlerConfiguration>();

        for (final var queue : queues) {
            final var endpoint = endpoints.get(
                queue.endpoint()
            );

            if (endpoint == null) {
                throw new JvgenUserException(
                    String.format(
                        "Endpoint [%s] for queue [%s] not found",
                        queue.endpoint(),
                        queue.name()
                    )
                );
            }

            final var keytab = keytabs.get(
                endpoint.keytab()
            );

            if (keytab == null) {
                throw new JvgenUserException(
                    String.format(
                        "Keytab [%s] for endpoint [%s] not found",
                        endpoint.keytab(),
                        endpoint.name()
                    )
                );
            }

            final var handler = this.createHandler(
                queue,
                endpoint,
                keytab
            );

            handlers.add(handler);
        }

        return handlers;
    }

    private DatabusHttpWorkerRabbitmqHandlerConfiguration createHandler(
        final DatabusWorkerRabbitmqQueueHandlerConfiguration queue,
        final DatabusHttpWorkerEndpointConfiguration endpoint,
        final DatabusHttpWorkerKeytabConfiguration keytab
    ) {
        return new DatabusHttpWorkerRabbitmqHandlerConfiguration() {
            @Override
            public String queue() {
                return queue.name();
            }

            @Override
            public DatabusHttpWorkerEndpointConfiguration endpoint() {
                return endpoint;
            }

            @Override
            public DatabusHttpWorkerKeytabConfiguration keytab() {
                return keytab;
            }
        };
    }
}
