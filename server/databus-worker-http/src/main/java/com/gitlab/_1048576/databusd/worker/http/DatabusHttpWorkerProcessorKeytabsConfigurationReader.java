package com.gitlab._1048576.databusd.worker.http;

import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;

public interface DatabusHttpWorkerProcessorKeytabsConfigurationReader {
    public DatabusHttpWorkerKeytabsConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception;
}
