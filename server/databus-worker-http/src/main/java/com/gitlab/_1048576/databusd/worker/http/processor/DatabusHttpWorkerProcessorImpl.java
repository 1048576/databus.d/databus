package com.gitlab._1048576.databusd.worker.http.processor;

import com.gitlab._1048576.databusd.worker.DatabusWorkerProcessor;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import java.io.IOException;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.kerberos.client.KerberosRestTemplate;
import org.springframework.web.client.ResponseErrorHandler;

public class DatabusHttpWorkerProcessorImpl implements DatabusWorkerProcessor {
    private static KerberosRestTemplate createRestTemplate(
        final String principal,
        final String keytabFilepath
    ) {
        final var result = new KerberosRestTemplate(
            keytabFilepath,
            principal
        );

        result.setErrorHandler(
            new ResponseErrorHandler() {
                @Override
                public boolean hasError(
                    final ClientHttpResponse response
                ) throws IOException {
                    return false;
                }

                @Override
                public void handleError(
                    final ClientHttpResponse response
                ) throws IOException {
                }
            }
        );

        return result;
    }

    private final String url;
    private final KerberosRestTemplate restTemplate;

    public DatabusHttpWorkerProcessorImpl(
        final String url,
        final String principal,
        final String keytabFilepath
    ) {
        this.url = url;
        this.restTemplate = createRestTemplate(principal, keytabFilepath);
    }

    @Override
    public void process(
        final byte[] body,
        final String contentType,
        final Map<String, ? extends Object> headers
    ) throws Exception {
        final var httpHeaders = this.createHttpHeaders(contentType);

        final var httpRequest = new HttpEntity<>(
            body,
            httpHeaders
        );

        final var response = this.restTemplate.exchange(
            this.url,
            HttpMethod.POST,
            httpRequest,
            String.class,
            headers
        );

        if (!response.getStatusCode().equals(HttpStatus.OK)) {
            throw new JvgenUserException(
                String.format(
                    "Status code [%s]",
                    response.getStatusCode()
                )
            );
        }
    }

    private HttpHeaders createHttpHeaders(final String contentType) {
        final var result = new HttpHeaders();

        result.setContentType(
            MediaType.valueOf(contentType)
        );

        return result;
    }
}
