package com.gitlab._1048576.libd.jvgen.kerberos;

import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;

public interface JvgenKerberosKeytabConfigurationReader {
    public JvgenKerberosKeytabConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception;
}
