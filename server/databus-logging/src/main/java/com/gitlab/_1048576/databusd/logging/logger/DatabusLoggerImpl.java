package com.gitlab._1048576.databusd.logging.logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gitlab._1048576.libd.jvgen.core.JvgenSystemException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabusLoggerImpl implements DatabusLogger {
    private final Logger logger;
    private final ObjectMapper objectMapper;

    public DatabusLoggerImpl() {
        this.logger = LoggerFactory.getLogger("message-log");
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public void messageProcessingCompleted(
        final String trackingId,
        final String routingKey,
        final Map<String, String> headers,
        final Integer size
    ) {
        final var entry = this.createEntry(
            routingKey,
            headers,
            true
        );

        entry.put("trackingId", trackingId);
        entry.put("size", size);

        try {
            this.logger.info(
                this.objectMapper.writeValueAsString(entry)
            );
        } catch (final JsonProcessingException e) {
            throw new JvgenSystemException(e);
        }
    }

    @Override
    public void messageProcessingFailed(
        final String routingKey,
        final Map<String, String> headers,
        final String reason
    ) {
        final var entry = this.createEntry(
            routingKey,
            headers,
            false
        );

        entry.put("reason", reason);

        try {
            this.logger.error(
                this.objectMapper.writeValueAsString(entry)
            );
        } catch (final JsonProcessingException e) {
            throw new JvgenSystemException(e);
        }
    }

    private ObjectNode createEntry(
        final String routingKey,
        final Map<String, String> headers,
        final Boolean processed
    ) {
        final var result = this.objectMapper.createObjectNode();

        result.set("headers", this.createHeadersNode(headers));
        result.put("processed", processed);

        return result;
    }

    private ObjectNode createHeadersNode(
        final Map<String, String> headers
    ) {
        final var result = this.objectMapper.createObjectNode();

        for (final var header : headers.entrySet()) {
            result.put(header.getKey(), header.getValue());
        }

        return result;
    }
}
