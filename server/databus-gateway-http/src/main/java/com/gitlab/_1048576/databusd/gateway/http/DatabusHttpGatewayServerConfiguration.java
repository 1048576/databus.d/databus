package com.gitlab._1048576.databusd.gateway.http;

import com.gitlab._1048576.libd.jvgen.httpserver.security.JvgenHttpServerSecurityAuthenticator;

public interface DatabusHttpGatewayServerConfiguration {
    public JvgenHttpServerSecurityAuthenticator createAuthenticator();
}
