package com.gitlab._1048576.databusd.gateway.http.server.security.userdetails;

import com.gitlab._1048576.databusd.gateway.http.server.security.DatabusHttpGatewayServerSecurityUserRole;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public final class DatabusHttpGatewayServerSecuritySimpleUserDetailsServiceImpl implements UserDetailsService {
    private final Collection<String> users;

    public DatabusHttpGatewayServerSecuritySimpleUserDetailsServiceImpl(
        final Collection<String> users
    ) {
        this.users = new ArrayList<>(users);
    }

    @Override
    public UserDetails loadUserByUsername(
        final String username
    ) throws UsernameNotFoundException {
        return new User(
            username,
            "",
            this.authorities(username)
        );
    }

    private List<SimpleGrantedAuthority> authorities(
        final String username
    ) {
        if (this.users.contains(username)) {
            return Arrays.asList(
                new SimpleGrantedAuthority(DatabusHttpGatewayServerSecurityUserRole.FULL.name())
            );
        } else {
            return Collections.emptyList();
        }
    }
}
