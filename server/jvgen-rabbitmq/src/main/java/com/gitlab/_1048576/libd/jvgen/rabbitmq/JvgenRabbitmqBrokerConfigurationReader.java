package com.gitlab._1048576.libd.jvgen.rabbitmq;

import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;

public interface JvgenRabbitmqBrokerConfigurationReader {
    public JvgenRabbitmqBrokerConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception;
}
