package com.gitlab._1048576.databusd.exporter.configuration;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.gitlab._1048576.databusd.configuration.AppConfiguration;
import com.gitlab._1048576.databusd.configuration.processor.ConfigurationProcessor;
import com.gitlab._1048576.databusd.configuration.processor.ConfigurationProcessorImpl;
import com.gitlab._1048576.databusd.exporter.configuration.entity.ExporterEntityConfiguration;
import com.gitlab._1048576.databusd.exporter.configuration.entity.ExporterEntityConfigurationImpl;

@Configuration
public class ExporterConfigurationImpl implements ExporterConfiguration {
    private final static ConfigurationProcessor<ExporterEntityConfiguration> entitiesNodeProcessor;

    static {
        entitiesNodeProcessor = new ConfigurationProcessorImpl<>(
            ExporterEntityConfigurationImpl::new
        );
    }

    private final Iterable<ExporterEntityConfiguration> entities;

    public ExporterConfigurationImpl(
        @Autowired
        final AppConfiguration appConfiguration
    ) throws IOException {
        this.entities = entitiesNodeProcessor.processToUniqueCollection(
            appConfiguration.detachObjectListNode("entities")
        );
    }

    @Override
    public Iterable<ExporterEntityConfiguration> entities() {
        return this.entities;
    }
}
