package com.gitlab._1048576.libd.jvgen.logging;

public class NotLoggingException extends Exception {
    public NotLoggingException(final Throwable cause) {
        super(cause);
    }
}
