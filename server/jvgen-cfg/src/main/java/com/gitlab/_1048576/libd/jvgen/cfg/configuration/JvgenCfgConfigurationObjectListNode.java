package com.gitlab._1048576.libd.jvgen.cfg.configuration;

public interface JvgenCfgConfigurationObjectListNode {
    public JvgenCfgConfigurationObjectNode detachObjectNode() throws Exception;

    public boolean isEmpty();

    public void close() throws Exception;
}
