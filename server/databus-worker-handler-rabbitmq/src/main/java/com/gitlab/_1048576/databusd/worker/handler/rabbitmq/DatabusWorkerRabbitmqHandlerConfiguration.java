package com.gitlab._1048576.databusd.worker.handler.rabbitmq;

import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBrokerConfiguration;

public interface DatabusWorkerRabbitmqHandlerConfiguration {
    public JvgenRabbitmqBrokerConfiguration broker();

    public Iterable<DatabusWorkerRabbitmqQueueHandlerConfiguration> queues();
}
