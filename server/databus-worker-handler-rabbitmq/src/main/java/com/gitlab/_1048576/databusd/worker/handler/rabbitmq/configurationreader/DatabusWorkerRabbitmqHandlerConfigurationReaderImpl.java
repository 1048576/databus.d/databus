package com.gitlab._1048576.databusd.worker.handler.rabbitmq.configurationreader;

import com.gitlab._1048576.databusd.worker.handler.rabbitmq.DatabusWorkerRabbitmqHandlerConfiguration;
import com.gitlab._1048576.databusd.worker.handler.rabbitmq.DatabusWorkerRabbitmqHandlerConfigurationReader;
import com.gitlab._1048576.databusd.worker.handler.rabbitmq.DatabusWorkerRabbitmqQueueHandlerConfiguration;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBrokerConfiguration;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBrokerConfigurationReader;
import com.gitlab._1048576.libd.jvgen.rabbitmq.configurationreader.JvgenRabbitmqBrokerConfigurationReaderImpl;
import java.util.HashMap;

public class DatabusWorkerRabbitmqHandlerConfigurationReaderImpl implements DatabusWorkerRabbitmqHandlerConfigurationReader {
    private final JvgenRabbitmqBrokerConfigurationReader brokerConfigurationReader;

    public DatabusWorkerRabbitmqHandlerConfigurationReaderImpl() {
        this.brokerConfigurationReader = new JvgenRabbitmqBrokerConfigurationReaderImpl();
    }

    @Override
    public DatabusWorkerRabbitmqHandlerConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var rabbitmqNode = node.detachObjectNode(
            "rabbitmq"
        );

        final var broker = this.brokerConfigurationReader.read(
            rabbitmqNode
        );
        final var queues = this.readQueues(rabbitmqNode);

        rabbitmqNode.close();

        return new DatabusWorkerRabbitmqHandlerConfiguration() {
            @Override
            public JvgenRabbitmqBrokerConfiguration broker() {
                return broker;
            }

            @Override
            public Iterable<DatabusWorkerRabbitmqQueueHandlerConfiguration> queues() {
                return queues;
            }
        };
    }

    private Iterable<DatabusWorkerRabbitmqQueueHandlerConfiguration> readQueues(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var queuesNode = node.detachObjectListNode(
            "queues"
        );

        final var queues = new HashMap<String, DatabusWorkerRabbitmqQueueHandlerConfiguration>();

        while (!queuesNode.isEmpty()) {
            final var queueNode = queuesNode.detachObjectNode();

            final var queue = this.readQueue(queueNode);

            queueNode.close();

            if (queues.put(queue.name(), queue) != null) {
                throw new JvgenUserException(
                    String.format(
                        "Queue [%s] is not unique",
                        queue.name()
                    )
                );
            }
        }

        queuesNode.close();

        return queues.values();
    }

    private DatabusWorkerRabbitmqQueueHandlerConfiguration readQueue(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var name = node.detachTextNode("name");
        final var endpoint = node.detachTextNode("endpoint");

        return new DatabusWorkerRabbitmqQueueHandlerConfiguration() {
            @Override
            public String name() {
                return name;
            }

            @Override
            public String endpoint() {
                return endpoint;
            }
        };
    }
}
