package com.gitlab._1048576.databusd.worker.handler.rabbitmq;

public interface DatabusWorkerRabbitmqQueueHandlerConfiguration {
    public String name();

    public String endpoint();
}
