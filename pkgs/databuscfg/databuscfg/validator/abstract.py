from typing import Generic
from typing import Protocol
from typing import TypeVar

Instance = TypeVar(
    name="Instance",
    contravariant=True
)


class Validator(Protocol, Generic[Instance]):
    def validate(self, instance: Instance) -> None:
        ...
