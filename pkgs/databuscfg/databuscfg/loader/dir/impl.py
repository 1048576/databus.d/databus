import os

from typing import Dict
from typing import Iterator
from typing import List
from typing import Tuple

from databuscfg.loader.file.impl import FileLoaderImpl
from pygentree.abstract import Tree
from pygentype.exception import ExpectedException


class DirLoaderImpl(Iterator[Tuple[str, Tree]]):
    _err_msg: str
    _dirs: Iterator[Tuple[str, List[str], List[str]]]
    _loaded: Dict[str, str]
    _curdir: Tuple[str, Iterator[str]]

    def __init__(
        self,
        path: str,
        err_msg: str
    ) -> None:
        self._err_msg = err_msg
        self._dirs = os.walk(os.path.abspath(path))
        self._loaded = {}
        self._curdir = ("", iter([]))

    def __next__(self) -> Tuple[str, Tree]:
        try:
            curdir_path, curdir_files = self._curdir
            curdir_file = next(curdir_files)
        except StopIteration:
            self._curdir = self._next_dir()

            return self.__next__()

        try:
            ext = ".yml"
            path = curdir_path + curdir_file
            try:
                if (not curdir_file.endswith(ext)):
                    raise ExpectedException(
                        msg="Unsupported file type. Only yaml"
                    )

                id = curdir_file[:-len(ext)]

                loaded_path = self._loaded.get(id)
                if (loaded_path is not None):
                    raise ExpectedException(
                        msg="[{}] already loaded from [{}]".format(
                            id,
                            loaded_path
                        )
                    )
                else:
                    self._loaded[id] = path
            except ExpectedException as e:
                raise e.wrap(
                    title="Cannot process [{}]".format(
                        path
                    )
                )
        except ExpectedException as e:
            raise e.wrap(self._err_msg)

        file_loader = FileLoaderImpl(
            path=path,
            err_msg_template=self._err_msg + " [{}]"
        )

        return (id, file_loader.load())

    def _next_dir(self) -> Tuple[str, Iterator[str]]:
        curdir_path, _, curdir_files = next(self._dirs)

        return (curdir_path + "/", iter(curdir_files))
