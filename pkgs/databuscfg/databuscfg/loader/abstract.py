from typing import Iterator
from typing import Tuple

from pygentree.abstract import Tree


class Loader:
    def load_item(self, item_id: str, err_msg: str) -> Tree:
        ...

    def load_collection(
        self,
        collection_id: str,
        err_msg: str
    ) -> Iterator[Tuple[str, Tree]]:
        ...
