from typing import Iterator
from typing import Tuple

from databuscfg.loader.abstract import Loader
from databuscfg.loader.dir.impl import DirLoaderImpl
from databuscfg.loader.file.impl import FileLoaderImpl
from pygenpath.abstract import PathResolver
from pygentree.abstract import Tree


class LoaderImpl(Loader):
    _resolver: PathResolver

    def __init__(self, resolver: PathResolver) -> None:
        self._resolver = resolver

    def load_item(self, item_id: str, err_msg: str) -> Tree:
        loader = FileLoaderImpl(
            path=self._resolver.resolve("./{}.yml".format(item_id)),
            err_msg_template=err_msg
        )

        return loader.load()

    def load_collection(
        self,
        collection_id: str,
        err_msg: str
    ) -> Iterator[Tuple[str, Tree]]:
        resolver = self._resolver.cd("./{}/".format(collection_id))

        return DirLoaderImpl(
            path=resolver.pwd(),
            err_msg=err_msg
        )
