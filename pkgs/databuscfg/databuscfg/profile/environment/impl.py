from typing import Dict

from databuscfg.profile.environment.abstract import EnvironmentProfile
from databuscfg.profile.zone.abstract import ZoneId
from databuscfg.profile.zone.abstract import ZoneProfile
from pygentree.abstract import Tree


class EnvironmentProfileImpl(EnvironmentProfile):
    @staticmethod
    def create(tree: Tree) -> EnvironmentProfile:
        with (tree):
            zones: Dict[ZoneId, ZoneProfile] = {}
            # zones_tree = tree.fetch_tree("zones")
            # for k, v in zones_tree.fetch_tree_items():
            #     zone_id = ZoneId(k)
            #     zone = ZoneProfileImpl.create(v)
            #     zones[zone_id] = zone

            return EnvironmentProfileImpl(
                zones=zones
            )

    _zones: Dict[ZoneId, ZoneProfile]

    def __init__(
        self,
        zones: Dict[ZoneId, ZoneProfile]
    ) -> None:
        self._zones = zones

    def zones(self) -> Dict[ZoneId, ZoneProfile]:
        return self._zones
