from typing import List

from databuscfg.profile.consumer.abstract import ConsumerProfile
from databuscfg.profile.node.abstract import NodeId
from pygentree.abstract import Tree


class ConsumerProfileImpl(ConsumerProfile):
    _nodes: List[NodeId]

    @staticmethod
    def create(tree: Tree) -> ConsumerProfile:
        with (tree):
            return ConsumerProfileImpl(
                nodes=list(
                    map(
                        NodeId,
                        tree.detach_text_set_node("nodes")
                    )
                )
            )

    def __init__(self, nodes: List[NodeId]) -> None:
        self._nodes = nodes

    def nodes(self) -> List[NodeId]:
        return self._nodes
