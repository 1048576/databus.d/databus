from typing import List

from databuscfg.profile.node.abstract import NodeId


class ConsumerProfile:
    def nodes(self) -> List[NodeId]:
        ...
