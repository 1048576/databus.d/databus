from __future__ import annotations

from typing import Dict

from databuscfg.profile.vhost.abstract import VhostId
from databuscfg.profile.zone.abstract import VhostProfile
from databuscfg.profile.zone.abstract import ZoneProfile
from pygentree.abstract import Tree


class ZoneProfileImpl(ZoneProfile):
    @staticmethod
    def create(tree: Tree) -> ZoneProfile:
        with (tree):
            vhosts: Dict[VhostId, VhostProfile] = {}
            # vhosts_tree = tree.fetch_tree("vhosts")
            # for k, v in vhosts_tree.fetch_tree_items():
            #     vhost_id = VhostId(k)
            #     vhost = VhostProfileImpl.create(v)
            #     vhosts[vhost_id] = vhost

        return ZoneProfileImpl(
            vhosts=vhosts
        )

    _vhosts: Dict[VhostId, VhostProfile]

    def __init__(self, vhosts: Dict[VhostId, VhostProfile]) -> None:
        self._vhosts = vhosts

    def vhosts(self) -> Dict[VhostId, VhostProfile]:
        return self._vhosts
