from typing import List

from databuscfg.profile.collection.abstract import CollectionId
from databuscfg.profile.collection.abstract import CollectionProfile
from databuscfg.profile.collection.abstract import CollectionProfileRoute
from databuscfg.profile.route.abstract import RouteId
from pygentree.abstract import Tree


class CollectionProfileRouteImpl(CollectionProfileRoute):
    @staticmethod
    def create(tree: Tree) -> CollectionProfileRoute:
        with (tree):
            return CollectionProfileRouteImpl(
                unique_id=RouteId(tree.detach_text_node("route"))
            )

    _unique_id: RouteId

    def __init__(self, unique_id: RouteId) -> None:
        self._unique_id = unique_id

    def unique_id(self) -> RouteId:
        return self._unique_id


class CollectionProfileImpl(CollectionProfile):
    @staticmethod
    def create(unique_id: str, tree: Tree) -> CollectionProfile:
        with (tree):
            return CollectionProfileImpl(
                unique_id=CollectionId(unique_id),
                routes=list(
                    map(
                        CollectionProfileRouteImpl.create,
                        tree.detach_tree_list_node("routes")
                    )
                )
            )

    _unique_id: CollectionId
    _routes: List[CollectionProfileRoute]

    def __init__(
        self,
        unique_id: CollectionId,
        routes: List[CollectionProfileRoute]
    ) -> None:
        self._unique_id = unique_id
        self._routes = routes

    def unique_id(self) -> CollectionId:
        return self._unique_id

    def routes(self) -> List[CollectionProfileRoute]:
        return self._routes
