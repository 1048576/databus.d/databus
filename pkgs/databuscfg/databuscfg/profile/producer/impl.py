from typing import List

from databuscfg.profile.node.abstract import NodeId
from databuscfg.profile.producer.abstract import ProducerProfile
from pygentree.abstract import Tree


class ProducerProfileImpl(ProducerProfile):
    _nodes: List[NodeId]

    @staticmethod
    def create(tree: Tree) -> ProducerProfile:
        with (tree):
            return ProducerProfileImpl(
                nodes=list(
                    map(
                        NodeId,
                        tree.detach_text_set_node("nodes")
                    )
                )
            )

    def __init__(self, nodes: List[NodeId]) -> None:
        self._nodes = nodes

    def nodes(self) -> List[NodeId]:
        return self._nodes
