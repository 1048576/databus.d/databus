from typing import Iterable
from typing import List

from databuscfg.configuration.abstract import Configuration
from databuscfg.configuration.wiki.abstract import WikiConfiguration
from databuscfg.profile.entity.abstract import EntityProfile
from databuscfg.profile.route.abstract import RouteProfile


class WikiConfigurationImpl(WikiConfiguration):
    _configuration: Configuration

    def __init__(self, configuration: Configuration) -> None:
        self._configuration = configuration

    def routes(self) -> Iterable[RouteProfile]:
        return sorted(self._configuration.routes().values())

    def entities(self) -> Iterable[EntityProfile]:
        return sorted(self._configuration.entities().values())

    def route_entities(
        self,
        route: RouteProfile
    ) -> List[EntityProfile]:
        entities: List[EntityProfile] = []
        for entity in self._configuration.entities().values():
            for entity_route in self.entity_routes(entity):
                if (route.unique_id() == entity_route.unique_id()):
                    entities.append(entity)

        return sorted(entities)

    def entity_routes(
        self,
        entity: EntityProfile
    ) -> List[RouteProfile]:
        return sorted(self._configuration.entity_routes(entity))
