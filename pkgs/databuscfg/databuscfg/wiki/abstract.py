from databuscfg.configuration.abstract import Configuration
from databuscfg.uploader.abstract import Uploader


class Wiki:
    def generate(
        self,
        configuration: Configuration,
        uploader: Uploader
    ) -> None:
        ...
