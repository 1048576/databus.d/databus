from typing import Dict
from typing import Iterator
from typing import List
from typing import Tuple

from databuscfg.loader.abstract import Loader
from pygentree.abstract import Tree
from pygentree.impl import TreeImpl
from pygentype.exception import ExpectedException


class LoaderMock(Loader):
    _collections: Dict[str, Dict[str, object]]
    _items: Dict[str, object]

    def __init__(
        self,
        collections: Dict[str, Dict[str, object]] = {},
        items: Dict[str, object] = {}
    ) -> None:
        self._collections = collections
        self._items = items

    def load_item(self, item_id: str, err_msg: str) -> Tree:
        nodes = self._items[item_id]

        try:
            return TreeImpl.create(nodes)
        except ExpectedException as e:
            raise e.wrap(
                title=err_msg
            )

    def load_collection(
        self,
        collection_id: str,
        err_msg: str
    ) -> Iterator[Tuple[str, Tree]]:
        try:
            collection = self._collections[collection_id]

            items: List[Tuple[str, Tree]] = []
            for item_id, item_content in collection.items():
                item = TreeImpl.create(item_content)
                items.append((item_id, item))

            return iter(items)
        except KeyError:
            raise ExpectedException(err_msg)
